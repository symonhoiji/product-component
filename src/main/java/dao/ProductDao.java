/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.*;
import java.util.ArrayList;
import com.mycompany.product_conponent.ProductPojo;

/**
 *
 * @author Kanny
 */
public class ProductDao implements DaoInterface<ProductPojo>{

    @Override
    public int add(ProductPojo object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        
        try {
            String sql = "INSERT INTO product (name,price)VALUES (?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            int row = stmt.executeUpdate();            
            ResultSet result = stmt.getGeneratedKeys();
            return result.getInt(1);
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        db.close();
        return -1;
       
    }

    @Override
    public ArrayList<ProductPojo> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
 
        try {
            
            String sql = "SELECT * FROM product";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                
                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                String img = result.getString("img");
                ProductPojo product = new ProductPojo(id,name,price,img);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        
        db.close();
        return list;
    }

    @Override
    public ProductPojo get(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
 
        try {
            
            String sql = "SELECT * FROM product WHERE id = "+id;
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()){
                
                int pid = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                ProductPojo product = new ProductPojo(pid,name,price);
                return product;
            }
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row=0;
        try {
            String sql = "DELETE FROM product WHERE id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();            
            
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        
        
        ///
        db.close();
        return row;
    }

    @Override
    public int update(ProductPojo object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row;
        try {
            String sql = "UPDATE product SET  name = ?,price = ? WHERE id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();            
            return row;
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        db.close();
        return 0;
    }

    public static void main(String[] args) {
        ProductDao dao = new ProductDao();  
        dao.delete(8);
    }
}
