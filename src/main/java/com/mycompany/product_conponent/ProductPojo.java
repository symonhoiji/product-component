/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.product_conponent;

import java.util.ArrayList;

/**
 *
 * @author Kanny
 */
public class ProductPojo {
    private int id;
    private String name;
    private double price;
    private String image;

   public ProductPojo(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public ProductPojo(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
     @Override
    public String toString() {
        return "ProductPojo{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
}
